#!/bin/python3

from http.server import BaseHTTPRequestHandler, HTTPServer

from cgi import parse_header, parse_multipart
from urllib.parse import parse_qs
import subprocess, sys


import json

def isrelease(commit):
	output = subprocess.run(['git', 'name-rev', commit['id']], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
	if output.returncode == 0:
		if output.stdout.endswith('release\n'):
			return True
	return False

def pull(commits):
	subprocess.run(['git','fetch'])
	if any(isrelease(x) for x in commits):
		subprocess.run(['git', 'pull'])
	else:
		print('No release commits')

class CI_RequestHandler(BaseHTTPRequestHandler):

	def parse_POST(self):
		length = int(self.headers['content-length'])
		return json.loads(self.rfile.read(length))

	def do_POST(self):

		data = self.parse_POST()

		if data['secret'] == sys.argv[1]:
			self.send_response(200)
			message = 'OK'
			print('Secret accepted.')
			if data['commits'] is not None:
				pull(data['commits'])
			else:
				print('No commits found.')
		else:
			self.send_response(401)
			message = "UNAUTHORIZED"
			print('Secret rejected.')

		self.send_header('Content-type','text/html')
		self.end_headers()

		self.wfile.write(bytes(message, "utf8"))
		return


def run():
	print('starting server...')

	# Server settings
	# Choose port 8080, for port 80, which is normally used for a http server, you need root access
	server_address = ('0.0.0.0', 8080)
	httpd = HTTPServer(server_address, CI_RequestHandler)
	print('running server...')
	httpd.serve_forever()


run()
